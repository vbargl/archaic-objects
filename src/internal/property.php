<?php

namespace Archaic\Objects\Internal;

function property_find($object, $propAlike, &$prop = null): bool {
  $vars = implode('|', array_filter(array_keys(get_class_vars(get_class($object)))));

  if (preg_match('/'.$propAlike.'/i', $vars, $match)) {
    $prop = $match[0];
    return true;
  }

  return false;
}
