<?php

namespace Archaic\Objects;

use function Archaic\Objects\Internal\property_find;

function populate(array $data, $object) {
  foreach ($data as $prop => $value) { # pair = [property, value] (can be associative)
    if (property_find($object, $prop, $found)) {
      $object->{$found} = $value;
    }
  }
  return $object;
}

function populateAll(array $data, string $class) {
  $res = [];
  foreach ($data as $ao) {
    $res[] = populate($ao, new $class);
  }
  return $res;
}

function populatePairs(array $pairs, $object) {
  foreach ($pairs as $pair) { # pair = [property, value] (can be associative)
    $keyvalue = array_values($pair);

    if (property_find($object, $keyvalue[0], $found)) {
      $object->{$found} = $keyvalue[1];
    }
  }
  return $object;
}